module RIO.Local
  ( module RIO
  , module RIO.Local
  , module RE
  ) where

import RIO

import Control.Monad.Trans.Resource as RE (ResourceT)
import GHC.Float as RE (double2Float, float2Double)
import RIO.State as RE (gets)
import Geomancy as RE

τ :: Float
τ = 2 * pi
