{-# LANGUAGE RecursiveDo #-}

module Stage.ReactiveBanana.Network.Stuff
  ( allocate
  , NetworkStuff
  ) where

import RIO.Local

import Data.StateVar (StateVar, makeStateVar)
import Engine.ReactiveBanana qualified as Network
import Engine.Types (StageRIO)
import Engine.Worker qualified as Worker
import Reactive.Banana qualified as RB
import Reactive.Banana.Combinators ((@>))
import Reactive.Banana.Frameworks qualified as RBF
import RIO.Vector.Storable qualified as Storable

import Stage.ReactiveBanana.World.Mesh qualified as Mesh

type NetworkStuff =
  ( Worker.Cell Double Text
  , Worker.Var Text
  , Worker.Var Bool
  , Data.StateVar.StateVar Int
  , Worker.Var Mesh.Output
  , RBF.Handler ()
  , Worker.Var Float
  , Worker.Var (Double, Double)
  , Worker.Var (Maybe Vec3)
  , Worker.Var (Storable.Vector Transform)
  , RBF.EventNetwork
  )

allocate :: ResourceT (StageRIO env) NetworkStuff
allocate = do
  (xAddHandler, fireX) <- liftIO RBF.newAddHandler
  void $! async do
    replicateM_ 5 do
      threadDelay 1e6
      getMonotonicTime >>= liftIO . fireX

  mkTimer1sec <- Network.timer 1e6

  rsFromNetwork <- Worker.spawnCell (mappend "[Cell] " . textDisplay) 0
  rsToNetwork <- Worker.newVar @_ @Text "change me"
  rsNetworkActuated <- Worker.newVar True

  rsCursorPos <- Worker.newVar (0, 0)
  rsPlanePos <- Worker.newVar Nothing
  rsPlaneCursor <- Worker.newVar mempty

  let initialInput = 1
  (meshInputAH, meshInputFire) <- liftIO RBF.newAddHandler
  meshInputVar <- newIORef initialInput
  let
    rsMeshInput = makeStateVar (readIORef meshInputVar) \x -> do
      writeIORef meshInputVar x
      meshInputFire x

  rsMeshOutput <- Worker.newVar $ Mesh.mkOutput 0 initialInput

  (frameAH, rsFrameEvent) <- liftIO RBF.newAddHandler
  rsFPS <- Worker.newVar 0.0

  rsNetwork <- Network.allocate True \(UnliftIO unlift) -> mdo
    xE <- RBF.fromAddHandler xAddHandler -- x as event
    xB <- RBF.fromChanges 0 xAddHandler  -- x as behavior

    -- run when xE fires
    RBF.reactimate $ fmap (unlift . logInfo . mappend "xE: " . displayShow) xE

    -- when xE fire, xB gets polled
    let xB_at_xE = fmap (round @_ @Integer) $ xB RB.<@ xE
    -- run when xB_at_xE fires
    RBF.reactimate $ fmap (unlift . logInfo . mappend "xB @ xE: " . displayShow) xB_at_xE

    -- convert behavior into event (causes a step delay due to "fire-in-reactimate" use)
    xBchangedE <- do
      (ebxE, ebxHandle) <- RBF.newEvent
      ebx <- RBF.changes xB
      RBF.reactimate' $ fmap (fmap ebxHandle) ebx
      pure ebxE

    RBF.reactimate $ fmap (unlift . logInfo . mappend "xB changedE: " . displayShow) xBchangedE

    -- external tick driver
    tickE <- mkTimer1sec

    -- snapshot of external world
    nowB <- RBF.fromPoll getMonotonicTime
    RBF.reactimate $ fmap (unlift . logInfo . mappend "nowB @ tickE: " . displayShow) (nowB RB.<@ tickE)

    -- Push x updates to `rsFromNetwork` worker input.
    Network.setWorkerInput rsFromNetwork $
      fmap (* 2) xE -- adjust event data slightly

    -- Poll `rsToNetwork` worker output
    varB <- RBF.fromPoll $ Worker.getOutputData rsToNetwork
    RBF.reactimate $ fmap (unlift . logInfo . mappend "varB @ tickE: " . displayShow) (varB RB.<@ tickE)

    -- Bidirectional component
    meshInputE <- RBF.fromAddHandler meshInputAH
    RBF.reactimate $ fmap (unlift . logInfo . mappend "meshInputE: " . displayShow) meshInputE -- log changes

    let meshOutputE = fmap (Mesh.mkOutput 0) meshInputE
    Network.setWorkerOutput rsMeshOutput meshOutputE
    RBF.reactimate $ fmap (unlift . logDebug . mappend "meshOutputE: " . displayShow) meshOutputE -- log changes

    frameE <- RBF.fromAddHandler frameAH
    frameCounter <- RB.accumB 1.0 $ fmap (const (+ 1)) frameE
    tickCounter <- RB.accumB 1.0 $ fmap (const (+ 1)) tickE
    RBF.reactimate $ tickE @> do
      numFrames <- frameCounter
      numTicks  <- tickCounter
      pure do
        let fps = numFrames / numTicks
        Worker.pushOutput rsFPS $ const fps

  pure
    ( rsFromNetwork
    , rsToNetwork
    , rsNetworkActuated
    , rsMeshInput
    , rsMeshOutput
    , rsFrameEvent
    , rsFPS
    , rsCursorPos
    , rsPlanePos
    , rsPlaneCursor
    , rsNetwork
    )
